import Apolloclient from 'apollo-boost';

const client = new Apolloclient({
  uri: 'https://countries.trevorblades.com/'
});

export default client;
