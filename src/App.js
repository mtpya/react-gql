import React from 'react';
import './App.css';
import { ApolloProvider } from '@apollo/react-hooks';
import client from './apolloClient';
import { Continents } from "./components";

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <Continents/>
      </div>
    </ApolloProvider>
  );
}

export default App;
