import React from 'react';
import { useQuery } from "@apollo/react-hooks";
import { queries } from '../../graphql';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExploreTwoToneIcon from '@material-ui/icons/ExploreTwoTone';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import { CountryModal } from '../index';

import './Continent.css';

function Continents() {
  const [state, setState] = React.useState({
    open: false,
    activeContinent: null,
    activeCountry: null
  });
  const { data } = useQuery(queries.CONTINENTS);

  const handleContinentItemClick = (continentCode) => {
    const { open, activeContinent, activeCountry } = state;

    setState({
      open: activeContinent !== continentCode || !open,
      activeContinent: continentCode,
      activeCountry
    });
  };

  const handleCountryItemClick = (code) => {
    setState({
      open: false,
      activeContent: null,
      activeCountry: code
    });
  };

  const handleCountryModalClose = () => {
    setState(state => ({
      ...state,
      activeCountry: null
    }));
  };

  const getNestedList = (list) => (
    <List className="continent__sub-list">
      {
        list.map(item => {
          const { code, name } = item;

          return (
              <ListItem
                divider
                key={code}
                onClick={() => handleCountryItemClick(code)}
                className="continent__sub-item">
                <ListItemIcon>
                  <AccountBalanceIcon fontSize="small"/>
                </ListItemIcon>
                <ListItemText primary={name} />
              </ListItem>
          );
        })
      }
    </List>
  );


  const getContinentsList = () => data && data.continents ? (
    <List>
      {
        data.continents.map(continent => {
            const { code, name } = continent;
            const { open, activeContinent } = state;
            const isOpen = open && activeContinent === code;

            return (
              <React.Fragment key={code}>
                <ListItem
                  button
                  className="continent__item"
                  onClick={() => handleContinentItemClick(code)}>
                  <ListItemIcon>
                    <ExploreTwoToneIcon fontSize="small"/>
                  </ListItemIcon>
                  <ListItemText primary={name} />
                  {isOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={isOpen} timeout="auto" unmountOnExit>
                  { getNestedList(continent.countries) }
                </Collapse>
              </React.Fragment>
            );
          }
        )
      }
    </List>
  ) : null;

  const getCountryInfo = () => {
    const { activeCountry } = state;

    return activeCountry ? (
      <CountryModal
        onModalClose={handleCountryModalClose}
        code={activeCountry} />
    ) : null;
  };

  return (
    <div className="countries">
      { getContinentsList() }
      { getCountryInfo() }
    </div>
  );
}

export default Continents;
