import React from 'react';
import { useQuery } from "@apollo/react-hooks";
import { queries } from '../../graphql';
import Modal from "@material-ui/core/Modal";
import Card from '@material-ui/core/Card';
import CardContent from "@material-ui/core/CardContent";

import './country-modal.css';

function CountryModal({ code, onModalClose }) {
  const { data } = useQuery(queries.COUNTRY, {
    variables: { code },
    skip: !code
  });
  const [open, setOpen] = React.useState(true);

  const handleClose = () => {
    setOpen(false);
    onModalClose();
  };

  return data ? (
      <Modal
        open={open}
        onClose={handleClose}
        className="country-modal">
        <Card className="country-modal__card">
          <CardContent>
            {
              Object.keys(data.country)
                .filter(key => key !== '__typename')
                .map(key => (
                <div key={key} className="country-modal__card-item">
                  <span>{key}:</span> {data.country[key]}
                </div>
              ))
            }
          </CardContent>
        </Card>
      </Modal>
    ) : <div/>;
}

export default CountryModal;
