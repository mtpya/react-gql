import { gql } from 'apollo-boost';

const CONTINENTS =  gql`
  query continents {
    continents {
      code
      name
      countries {
        code
        name
        continent {
          code
        }
      }
    }
  }
`;

const COUNTRY = gql`
  query country($code: String!) {
    country(code: $code) {
      name
      native
      phone
      currency
    }
  }
`;

export {
  CONTINENTS,
  COUNTRY
};